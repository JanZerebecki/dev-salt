.PHONY: apply
apply:
	salt-ssh '*' state.apply 2>&1 | tee var/log/salt-output/$(shell date -Is).txt
