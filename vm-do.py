#!/usr/bin/python3
import logging
import random
import sys
from sh import openstack

def create():
  name = 'example-vm-host'
  image = 'example-image'
  flavor = 'm1.medium'
  keyname = 'example key'
  netname = 'example-net'
  openstack('--os-cloud', 'engcloud', 'server', 'create', '--flavor', flavor, '--image', image, '--security-group', 'ping/ssh', '--key-name', keyname, '--network', netname, '--wait', name, _out=sys.stdout, _err=sys.stderr)
  ips = [ l.strip() for l in openstack('--os-cloud', 'engcloud', 'floating', 'ip', 'list', '--status', 'DOWN', '-c', 'Floating IP Address', '-f', 'value', _iter=True) ]
  floatingip = random.choice(ips)
  openstack('--os-cloud', 'engcloud', 'server', 'add', 'floating', 'ip', name, floatingip, _out=sys.stdout, _err=sys.stderr)
  # write name to roster
  entry = "%s:\n  host: %s\n  username: opensuse\n  sudo: True\n".format(name, floatingip)
  with open("roster", "a") as f:
      f.write(entry)
  logging.info("Wrote to roster:\n%s", entry)
  sh.salt-ssh('-ltrace', '--ignore-host-keys', name, '-r', 'echo pong', _out=sys.stdout, _err=sys.stderr)
  sh.salt-ssh(name, '-r', 'zypper --non-interactive --verbose dup && zypper --non-interactive kernel-default && zypper --non-interactive --verbose dup && touch /etc/salt-last-dist-upgrade', _out=sys.stdout, _err=sys.stderr)
  openstack('--os-cloud', 'engcloud', 'server', 'reboot', '--soft', '--wait', name, _out=sys.stdout, _err=sys.stderr)
  sh.curl('--verbose', '--retry-connrefused', '--retry', '20', 'sftp://opensuse@'+floatingip+'/etc/os-release', _out=sys.stdout, _err=sys.stderr)
  sh.salt-ssh(name, 'state.apply', _out=sys.stdout, _err=sys.stderr)

def main():
  logging.basicConfig(level=logging.INFO)
  create()
  logging.info('Done.')

if __name__ == "__main__":
  main()

