kubeadm:
  pkg.installed:
    - pkgs:
        - patterns-containers-kubeadm
        # needed instead of kernel-default-base as wave needs kernel module openvswitch
        - kernel-default
        - kubevirt-manifests
        - kubevirt-virtctl

# ensure prerequisite which are always there on the image we use
# TODO run during image ci
#swap disabled: cat /proc/swaps
#cat /sys/class/dmi/id/product_uuid
#ls -la /etc/alternatives/iptables
#zypper search nftables

/etc/modules-load.d/99-kubernetes-crio.conf:
  file.managed:
    - contents: |
        overlay
        br_netfilter
  service.running:
    - name: systemd-modules-load.service
    - watch:
      - file: /etc/modules-load.d/99-kubernetes-crio.conf
/etc/sysctl.d/99-kubernetes-cri.conf:
  file.managed:
    - contents: |
        net.bridge.bridge-nf-call-iptables  = 1
        net.ipv4.ip_forward                 = 1
        net.bridge.bridge-nf-call-ip6tables = 1
  service.running:
    - name: systemd-sysctl.service
    - watch:
      - file: /etc/sysctl.d/99-kubernetes-cri.conf

/etc/kubernetes:
  file.directory: []
crio:
  service.running:
    - enable: True
kubelet:
  service.enabled: []

/etc/kubernetes/kubeadm-init-log:
  file.managed:
    - mode: 0600
"kubeadm config images pull && touch /etc/kubernetes/kubeadm-images-pull-done":
  cmd.run:
    - creates: /etc/kubernetes/kubeadm-images-pull-done
#TODO just always add for possible high availability --control-plane-endpoint
"kubeadm init 2>&1 | tee -a /etc/kubernetes/kubeadm-init-log":
  cmd.run:
    - creates: /etc/kubernetes/admin.conf
#TODO get the join command from init log or by issuing other commands
#TODO kubelet service start?
/root/.kube/config:
  file.symlink:
    - target: /etc/kubernetes/admin.conf
    - makedirs: True

# TODO everything below here should probably be done with a tool that syncs a k8s install from git
# TODO make kubeadm init be able to kubectl apply the files linked in a directory

# for test system with fewer nodes where other things should share the master nodes
"kubectl taint nodes --all node-role.kubernetes.io/master- && touch /etc/kubernetes/kubectl-taint-nodes-remove-master-done":
  cmd.run:
    - creates: /etc/kubernetes/kubectl-taint-nodes-remove-master-done

"kubectl apply -f /usr/share/k8s-yaml/weave/weave.yaml && touch /etc/kubernetes/kubectl-apply-weave-done":
  cmd.run:
    - creates: /etc/kubernetes/kubectl-apply-weave-done

"kubectl apply -f /usr/share/kube-virt/manifests/release/kubevirt-operator.yaml && kubectl apply -f /usr/share/kube-virt/manifests/release/kubevirt-cr.yaml && touch /etc/kubernetes/kubectl-apply-kubevirt-done"
  cmd.run:
    - creates: /etc/kubernetes/kubectl-apply-kubevirt-done
