include:
  - config.motd

#specific for image openSUSE-Tumbleweed-JeOS.x86_64 TODO be more generic
repo-non-oss:
  pkgrepo.managed:
    - refresh: False
repo-oss:
  pkgrepo.managed:
    - refresh: False
repo-update:
  pkgrepo.managed:
    - refresh: False

"zypper --non-interactive --verbose dup && zypper --non-interactive in kernel-default && zypper --non-interactive --verbose dup && touch /etc/salt-last-dist-upgrade":
  cmd.run:
    - creates: /etc/salt-last-dist-upgrade

common-packages:
  pkg.installed:
    - pkgs:
        - screen
        - lsof


